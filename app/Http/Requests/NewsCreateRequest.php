<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title' => 'required|min:3|max:50',
            "announcement" =>'required|min:3|max:100',
            'text' =>'required|min:5',
            'rubric_id' =>'required|integer',
            'author_id' =>'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'announcement.required' => 'A announcement is required',
            'text.required' => 'A text is required',
            'rubric_id.required' => 'A rubric_id is required',
            'author_id.required' => 'A author_id is required',
        ];
    }
}
