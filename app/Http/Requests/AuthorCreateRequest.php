<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => 'required|min:3|max:40',
            "surname" =>'required|min:3|max:40',
            'email' =>'required|min:5|max:100',
            'avatar_url' =>'nullable|max:255',
            // 'password' =>'required|',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'surname.required' => 'A surname is required',
            'email.required' => 'A email is required',
            // 'password.required' => 'A password is required',
        ];
    }
}
