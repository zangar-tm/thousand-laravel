<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RubricCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => 'required|unique:rubrics|max:255',
            "parent_id" =>'integer|nullable',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
        ];
    }
}
