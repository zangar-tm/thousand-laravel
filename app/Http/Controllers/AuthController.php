<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AuthorCreateRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return "Signed in";
        }

        return "Incorrect credentials";
    }

    public function customRegistration(AuthorCreateRequest $request)
    {
        $validated = $request->validated();
        $this->create($validated);
        return response()->json("Registered", 201);
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            // 'surname' => $data['surname'],
            'email' => $data['email'],
            // 'avatar_url' => $data['avatar_url'],
            'password' => Hash::make($data['password'])
        ]);
    }

}
