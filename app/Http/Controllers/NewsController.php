<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests\NewsCreateRequest;
use App\Models\News;
use App\Models\Author;
class NewsController extends Controller
{
    public function index()
    {
        return News::all();
    }

    public function store(NewsCreateRequest $request)
    {
        $validated = $request->validated();
        $res = News::create($validated);
        // Logs are saved in storage/logs/mailer.log file
        $email = Author::find($request->input('author_id'))->email;
        Log::channel('mailer')->info("From ABC: ", ['whom' => $email, 'message' =>"Thank you for your article: ", 'title'=>$res->title]);
        return response()->json($res, 201);
    }

    public function getNews($id)
    {
        return News::find($id);
    }

    public function findNews($title)
    {
        return News::where('title', $title)->first();
    }
}
