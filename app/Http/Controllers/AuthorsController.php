<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AuthorCreateRequest;
use App\Models\Author;
use App\Models\News;
class AuthorsController extends Controller
{
    public function index()
    {
        return Author::all();
    }

    public function store(AuthorCreateRequest $request){
        $validated = $request->validated();
        $res = Author::create($validated);
        return response()->json($res, 201);
    }

    public function getAuthor($id)
    {
        return Author::find($id);
    }


    public function authorsNews($id)
    {
        return News::where('author_id', $id)->get();
    }
}
