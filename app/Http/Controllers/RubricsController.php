<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rubric;
use App\Models\News;
use App\Http\Requests\RubricCreateRequest;
class RubricsController extends Controller
{
    public function index()
    {
        return Rubric::all();
    }

    public function store(RubricCreateRequest $request){
        $validated = $request->validated();
        $res = Rubric::create($validated);
        return response()->json($res, 201);
    }

    public function rubricsNews($id)
    {
        $data = Rubric::with(['rubric_news', 'childrenRubrics.rubric_news','childrenRubrics.childrenRubrics.rubric_news'])->where('id', $id)->get()->toArray();
        $flatten = $this->flatten($data);

        foreach ($flatten as $key => $fl) {
            if (!array_key_exists('rubric_id', $fl)) {
                unset($flatten[$key]);
            }
        }
        return array_values($flatten);
    }

    public function searchNewsInRubric($id, $title){
        $news = $this->rubricsNews($id);
        foreach ($news as $article)
        {
            if ($article['title'] == $title)
            {
                return $article;
            }
            return "Article not found";
        }
    }

    public function flatten($array)
    {
        $flatArray = [];

        if (!is_array($array)) {
            $array = (array)$array;
        }

        foreach($array as $key => $value) {
            if (is_array($value) || is_object($value)) {
                $flatArray = array_merge($flatArray, $this->flatten($value));
            } else {
                $flatArray[0][$key] = $value;
            }
        }
        return $flatArray;
    }

}
