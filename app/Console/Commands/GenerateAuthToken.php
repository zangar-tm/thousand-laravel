<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class GenerateAuthToken extends Command
{

    protected $signature = 'make:token';

    protected $description = 'Generates a new api client auth token to consume our api';


    public function handle()
    {

        $token = Str::random(60);

        DB::table('api_clients')->whereNotnull('api_token')->delete();

        DB::table('api_clients')->insert([
            'api_token' => hash('sha256', $token),
        ]);

        $this->info($token);

        return 0;
    }
}
