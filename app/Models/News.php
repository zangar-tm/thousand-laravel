<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $table = 'news';

    protected $fillable = [
        'title',
        'announcement',
        'text',
        'rubric_id',
        'author_id',
    ];

    public function rubrics(){
        return $this->belongsTo(Rubric::class, 'rubric_id', 'id');
    }

    public function author(){
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }
}
