<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $table = 'authors';

    protected $fillable = [
        'name',
        'surname',
        'email',
        'avatar_url',
    ];

    public function authors_news(){
        return $this->hasMany(News::class);
    }
}
