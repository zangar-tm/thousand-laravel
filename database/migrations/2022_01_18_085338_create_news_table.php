<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rubric_id');
            $table->unsignedBigInteger('author_id');
            $table->string('title');
            $table->string('announcement');
            $table->text('text');
            // $table->foreign('rubric_id')
            //             ->references('id')
            //             ->on('rubrics')
            //             ->onDelete('cascade');
            // $table->foreign('author_id')
            //             ->references('id')
            //             ->on('authors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
