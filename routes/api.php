<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RubricsController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\AuthorsController;
// use App\Http\Controllers\AuthController;

Route::get('/rubrics', [RubricsController::class, 'index']); // get all rubrics
Route::get('/all-news', [NewsController::class, 'index']); // get all news


// Token create command "php artisan make:token", then one should put this token to check
Route::middleware(['auth:api'])->group(function () {
    Route::get('/rubrics/{id}/news', [RubricsController::class, 'rubricsNews']);// выдача списка всех новостей, которые относятся к указанной рубрике
    Route::post('/rubrics', [RubricsController::class, 'store']);               // Добавить Рубрику
    Route::get('/rubrics/{id}/{title}', [RubricsController::class, 'searchNewsInRubric']); // искать новости по рубрике, включая дочерние

    Route::get('/all-news/{id}', [NewsController::class, 'getNews']);           // выдача информации о статьях по их идентификаторам
    Route::get('/all-news/{title}', [NewsController::class, 'findNews']);       // поиск новости по названию
    Route::post('/all-news', [NewsController::class, 'store']);                 // Добавить новости

    Route::get('/authors', [AuthorsController::class, 'index']);                // выдача списка авторов
    Route::get('/authors/{id}', [AuthorsController::class, 'getAuthor']);       // get author by ID
    Route::get('/authors/{id}/news', [AuthorsController::class, 'authorsNews']);// выдача всех новостей конкретного автора
    Route::post('/authors', [AuthorsController::class, 'store']);               // Добавить Автора
});


// Did not understood the instruction -> "Базовая token-based авторизация.", therefore created also other routes for registration and login, but One should uncommend user_model, user_migration, and below routes to work with.

// Route::post('/register', [AuthController::class, 'customRegistration']);
// Route::post('/login', [AuthController::class, 'customLogin']);


